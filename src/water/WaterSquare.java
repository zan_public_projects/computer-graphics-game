package water;

public class WaterSquare {

    public static final float TILE_SIZE = 50;

    private float height;
    private float x,z;

    public WaterSquare(float centerX, float centerZ, float height){
        this.x = centerX;
        this.z = centerZ;
        this.height = height;
    }

    public float getHeight() {
        return height;
    }

    public float getX() {
        return x;
    }

    public float getZ() {
        return z;
    }

    public void setHeight(float height) {
        this.height = height;
    }
}