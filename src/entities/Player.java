package entities;

import models.TexturedModel;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;
import renderingEngine.DisplayManager;
import terrains.Terrain;
import tools.Music;

public class Player extends Entity {
    private static final String CHAINSAW_SOUND = "chainsaw";
    private static final String BACKGROUND_MUSIC = "background_music";

    private static final float RUN_SPEED = 15;
    private static final float TURN_SPEED = 80;
    private static final float GRAVITY = -50;
    private static final float JUMP_POWER = 20;
    private static final float LEVITATION = 1.4f;
    private static final float TERRAIN_OFFSET = 9f;     //TODO remove when "naklon klanca" checking is done
    private int collisionVector = 1;

    private float currentSpeed = 0;
    private float currentTurnSpeed = 0;
    private float upwardsSpeed = 0;
    private boolean in_air = false;
    private int wood_count = 0;

    private boolean hasChainsaw = false;
    private int cuttingSpeed = 1;
    private final int sound_len = 1;

    public Player(TexturedModel model, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
        super(model, position, rotX, rotY, rotZ, scale, false);
    }

    public void move(Terrain terrain, Entity entity, Music music_player){
        checkInputs();

        // Movement
        super.increaseRotation(0, currentTurnSpeed * DisplayManager.getFrameTimeSeconds(), 0);
        float distance = currentSpeed * DisplayManager.getFrameTimeSeconds();
        float dx = (float) (distance * Math.sin(Math.toRadians(super.getRotY()))) * this.collisionVector;
        float dz = (float) (distance * Math.cos(Math.toRadians(super.getRotY()))) * this.collisionVector;

        super.increasePosition(dx, 0, dz);

        // Jumping
        upwardsSpeed += GRAVITY * DisplayManager.getFrameTimeSeconds();
        super.increasePosition(0, upwardsSpeed * DisplayManager.getFrameTimeSeconds(), 0);

        float terrainHeight = terrain.getHeightOfTerrain(super.getPosition().x, super.getPosition().z);



        // Handling falling
        if(super.getPosition().y < terrainHeight + LEVITATION && entity == null || (entity != null && !entity.isCan_stand())){
            upwardsSpeed = 0;
            in_air = false;
            super.setPosition(new Vector3f(super.getPosition().x, terrainHeight + LEVITATION, super.getPosition().z));
        }

        if(entity != null && entity.isCan_stand()){
            float y = entity.getSIZE_Y() + entity.getPosition().y;
            super.setPosition(new Vector3f(super.getPosition().x, y, super.getPosition().z));
        }

        if(!music_player.getCurrent_song().equals(BACKGROUND_MUSIC) &&
                ((System.nanoTime()/ 1000000000.0) - this.previous_time > sound_len)){
            music_player.stop();
            music_player.song_player(BACKGROUND_MUSIC);
            music_player.play();
            music_player.loop();
        }


        checkEdgeOfTerrain(terrain);
    }

    private void checkEdgeOfTerrain(Terrain terrain){
        if( super.getPosition().x  <= 0 || super.getPosition().z  <= 0){
            this.currentSpeed = 0;
            this.currentTurnSpeed = 0;
            this.upwardsSpeed = 0;
            super.setRotY((super.getRotY() + 10) % 360);

            float x = super.getPosition().x + TERRAIN_OFFSET;
            float z = super.getPosition().z + TERRAIN_OFFSET;
            float y = terrain.getHeightOfTerrain(x, z);

            super.setPosition(new Vector3f( x, y, z));
        }

        else if(super.getPosition().x >= 800 || super.getPosition().z >= 800){
            this.currentSpeed = 0;
            this.currentTurnSpeed = 0;
            this.upwardsSpeed = 0;

            super.setRotY((super.getRotY() + 10) % 360);
            float x = super.getPosition().x - TERRAIN_OFFSET;
            float z = super.getPosition().z - TERRAIN_OFFSET;
            float y = terrain.getHeightOfTerrain(x, z);

            super.setPosition(new Vector3f( x, y, z));
        }
    }


    private void jump(){
        if(!in_air){
            this.upwardsSpeed = JUMP_POWER;
            in_air = true;
        }
    }


    public void checkInputs(){
        if(Keyboard.isKeyDown(Keyboard.KEY_W)){
            this.currentSpeed = RUN_SPEED;
        }
        else if(Keyboard.isKeyDown(Keyboard.KEY_S)){
            this.currentSpeed = -RUN_SPEED;
        }
        else{
            this.currentSpeed = 0;
        }

        if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)){
            sprint();
        }

        if(Keyboard.isKeyDown(Keyboard.KEY_D)){
            this.currentTurnSpeed = -TURN_SPEED;
        }
        else if(Keyboard.isKeyDown(Keyboard.KEY_A)){
            this.currentTurnSpeed = TURN_SPEED;
        }
        else{
            this.currentTurnSpeed = 0;
        }

        if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
            jump();
            sprintJump();
        }
    }

    private void sprint(){
        if(!in_air){
            this.currentSpeed = RUN_SPEED * 3;
        }
    }
    private void sprintJump(){
        if(this.currentSpeed == RUN_SPEED * 3){
            this.upwardsSpeed =  JUMP_POWER * 1.3f;
        }
    }

    public float getY(){
        return this.getPosition().y;
    }

    public void woodMinusOne() {
        this.wood_count--;
    }

    public void woodPlusOne() {
        this.wood_count++;
    }

    public int getWood_count() {
        return wood_count;
    }

    public void collisionVector(int num){
        this.collisionVector = num;
    }

    public void zeroSpeed(){
        this.currentSpeed = 0;
    }

    public boolean getHasChainsaw() {
        return hasChainsaw;
    }

    public void setHasChainsaw(boolean hasChainsaw) {
        this.hasChainsaw = hasChainsaw;
        this.cuttingSpeed = 4;
    }

    public int getCuttingSpeed(Music music_player) {
        double current_time = System.nanoTime() / 1000000000.0;

        if(this.hasChainsaw && ((this.previous_time == 0) || (current_time - this.previous_time > sound_len))){
            this.previous_time = current_time;
            music_player.stop();
            music_player.song_player(CHAINSAW_SOUND);
            music_player.play();
        }

        return cuttingSpeed;
    }

}
