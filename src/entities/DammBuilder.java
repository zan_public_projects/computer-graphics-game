package entities;

import models.Model;
import models.TexturedModel;
import objConverter.ModelData;
import objConverter.OBJFileLoader;
import org.lwjgl.util.vector.Vector3f;
import renderingEngine.Loader;
import textures.ModelTexture;
import water.WaterSquare;

import java.util.ArrayList;
import java.util.List;

public class DammBuilder{
    public ArrayList<float[]> LOG_COORDINATES_LIST  = new ArrayList<>();
    private final float [] LAKE_HEIGHT = {-13.7f, -11.5f, -9f, -6.5f, -4f};

    private final int [] LOG_LVL = {16, 22, 26, 30, 33};
    private final int [] LOG_SUM = {16, 38, 64, 94, 127};

    private int log_num = 0;
    private TexturedModel model;
    public CirclePicker circlePicker;

    public DammBuilder(Loader loader, List<Entity> entityList) {
        ModelData testing = OBJFileLoader.loadOBJ("log");
        Model raw_log = loader.loadToVAO(testing.getVertices(), testing.getTextureCoords(), testing.getNormals(),
                testing.getIndices());
        this.model = new TexturedModel(raw_log, new ModelTexture(loader.loadTexture("log")));

        fill_logCoordinates();

        this.circlePicker = new CirclePicker(loader);
        this.circlePicker.placeCircle(entityList, LOG_COORDINATES_LIST.get(0));
    }

    public void fill_logCoordinates(){
        for (int i = 0; i < LOG_LVL[0]; i++) {  // first height level   -13.5
            float[] temp = {392 + (3 * i), -14f, 360};
            LOG_COORDINATES_LIST.add(temp);
        }

        for (int i = 0; i < LOG_LVL[1]; i++) {  // first height level   -11.5
            float[] temp = {387.5f + (3 * i), -11.5f, 360};
            LOG_COORDINATES_LIST.add(temp);
        }

        for (int i = 0; i < LOG_LVL[2]; i++) {  // first height level   -9
            float[] temp = {383f + (3 * i), -9f, 360};
            LOG_COORDINATES_LIST.add(temp);
        }

        for (int i = 0; i < LOG_LVL[3]; i++) {  // first height level   -6.5
            float[] temp = {378.5f + (3 * i), -6.5f, 360};
            LOG_COORDINATES_LIST.add(temp);
        }

        for (int i = 0; i < LOG_LVL[4]; i++) {  // first height level   -4
            float[] temp = {374f + (3 * i), -4f, 360};
            LOG_COORDINATES_LIST.add(temp);
        }
    }


    public void placeLogAndNewCircle(List<Entity> entities, List<WaterSquare> waterSquareList){
        if(this.log_num < LOG_COORDINATES_LIST.size()){

            this.circlePicker.removeCircleAndLight(entities);
            Entity entity = new Entity(this.getModel(), new Vector3f(LOG_COORDINATES_LIST.get(log_num)[0], LOG_COORDINATES_LIST.get(log_num)[1],
                    LOG_COORDINATES_LIST.get(log_num)[2]), 0,0,0,1.5f, 0.5f,  3,  5, false, true);
            entities.add(entity);
            this.log_num++;
            checkWaterLevel(waterSquareList);

            if(this.log_num < LOG_COORDINATES_LIST.size()){
                circlePicker.placeCircle(entities, LOG_COORDINATES_LIST.get(log_num));
            }
        }
    }

    private void checkWaterLevel(List<WaterSquare> waterSquareList){
        if(this.log_num >= this.LOG_SUM[4]){
            loop_water(waterSquareList, this.LAKE_HEIGHT[4]);
        }
        else if(this.log_num >= this.LOG_SUM[3]){
            for (int i = 0; i < 6; i++) {
                waterSquareList.get(i).setHeight(this.LAKE_HEIGHT[3]);
            }
        }
        else if(this.log_num >= this.LOG_SUM[2]){
            for (int i = 0; i < 6; i++) {
                waterSquareList.get(i).setHeight(this.LAKE_HEIGHT[2]);
            }
        }
        else if(this.log_num >= this.LOG_SUM[1]){
            for (int i = 0; i < 6; i++) {
                waterSquareList.get(i).setHeight(this.LAKE_HEIGHT[1]);
            }
        }
        else if(this.log_num >= this.LOG_SUM[0]){
            for (int i = 0; i < 6; i++) {
                waterSquareList.get(i).setHeight(this.LAKE_HEIGHT[0]);
            }
        }
    }

    private void loop_water(List<WaterSquare> waterSquareList, float height){
        for (int i = 0; i < 6; i++) {
            waterSquareList.get(i).setHeight(height);
        }
    }

    public TexturedModel getModel() {
        return model;
    }

    public boolean isMaxLogs() {
        return this.log_num >= 127;
    }
}
