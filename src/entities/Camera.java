package entities;

import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;

public class Camera {
    private Vector3f position = new Vector3f(0,20,20);
    private float pitch = 30;        // determines how high the camera is
    private float yaw = 50;
    private float roll;

    private Player player;
    private float distanceFromPlayer = 15;
    private float angleAroundPlayer = 0;

    //TODO add limitations to what the player can do with the camera, max - min zoom
    public Camera(Player player) {
        this.player = player;
        position = new Vector3f(player.getRotX(), player.getRotY(), player.getRotZ());
    }

    public void move(){
        calculateZoom();
        calculatePitch();
        calculateAngleAroundPlayer();

        float horizontalDistance = calculateHorizontalDistance();
        float verticalDistance = calculateVerticalDistance();

        calculateCameraPosition(horizontalDistance, verticalDistance);
        this.yaw = 180 - (player.getRotY() + angleAroundPlayer);
        yaw %= 360;
    }

    public Vector3f getPosition() {
        return position;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public float getRoll() {
        return roll;
    }

    private void calculateCameraPosition(float horizontalDistance, float verticalDistance){
        float theta = player.getRotY() + angleAroundPlayer;
        float offsetX = (float) (horizontalDistance * Math.sin(Math.toRadians(theta)));
        float offsetZ = (float) (horizontalDistance * Math.cos(Math.toRadians(theta)));

        position.x = player.getPosition().x - offsetX;
        position.z = player.getPosition().z - offsetZ;
        position.y = player.getPosition().y + verticalDistance;
    }

    private void calculateZoom(){
        float zoomLevel = Mouse.getDWheel() * 0.01f;
        if(distanceFromPlayer < 40 && distanceFromPlayer > 5){
            distanceFromPlayer -= zoomLevel;
        }
        else{
            distanceFromPlayer = 20;
        }
    }

    private void calculatePitch(){
        if(Mouse.isButtonDown(1)){      // 1 is for the right mouse b, 0 is for left
            float pitchChange = Mouse.getDY() * 0.1f;
            pitch -= pitchChange;
            if(pitch >= 10 && pitch <= 100){
                pitch -= pitchChange;
            }
            else{
                pitch += pitchChange;
            }
        }
    }

    private void calculateAngleAroundPlayer(){
        if(Mouse.isButtonDown(0)) {
            float angleChange = Mouse.getDX() * 0.2f;
            angleAroundPlayer -= angleChange;
        }
    }

    private float calculateHorizontalDistance(){
        return (float) (distanceFromPlayer * Math.cos(Math.toRadians(pitch)));
    }

    private float calculateVerticalDistance(){
        return (float) (distanceFromPlayer * Math.sin(Math.toRadians(pitch)));
    }

    public void invertPitch(){
        this.pitch = -pitch;
    }
}