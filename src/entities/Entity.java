package entities;

import models.TexturedModel;
import org.lwjgl.util.vector.Vector3f;

public class Entity {
    private float DISTANCE_CHECK_COLLISION = 4;
    private float distance_from_player = 0;
    private boolean player_in_range = false;
    private boolean wood = false;           // if this is true you can destroy an entity
    private boolean can_stand = false;
    private boolean chainsaw = false;

    private float SIZE_X = 1;
    private float SIZE_Y = 0;
    private float SIZE_Z = 1;

    public double hp_time = 2000;    // in milliseconds
    public double previous_time = 0;

    private TexturedModel model;
    private Vector3f position;
    private float rotX, rotY, rotZ;
    private float scale;

    private int textureIndex = 0;

    public Entity(TexturedModel model, Vector3f position, float rotX, float rotY,
                  float rotZ, float scale, boolean wood) {
        this.model = model;
        this.position = position;
        this.rotX = rotX;
        this.rotY = rotY;
        this.rotZ = rotZ;
        this.scale = scale;
        this.wood = wood;
    }

    public Entity(TexturedModel model, Vector3f position, float rotY, float scale, boolean wood, float size_x,
                  float size_y, float size_z) {
        this.model = model;
        this.position = position;
        this.rotY = rotY;
        this.scale = scale;
        this.wood = wood;
        this.SIZE_X = size_x;
        this.SIZE_Y = size_y;
        this.SIZE_Z = size_z;

    }

    public Entity(TexturedModel model, Vector3f position, boolean chainsaw, float rotY, float scale, boolean wood, float size_x, float size_z) {
        this.model = model;
        this.position = position;
        this.rotY = rotY;
        this.scale = scale;
        this.wood = wood;
        this.chainsaw = chainsaw;
        this.SIZE_X = size_x;
        this.SIZE_Z = size_z;
    }


    public Entity(TexturedModel model, int index, Vector3f position, float rotX, float rotY,
                  float rotZ, float scale, boolean wood) {
        this.model = model;
        this.position = position;
        this.rotX = rotX;
        this.rotY = rotY;
        this.rotZ = rotZ;
        this.scale = scale;
        this.textureIndex = index;
        this.wood = wood;
    }

    public Entity(TexturedModel model, Vector3f position, float rotX, float rotY,
                  float rotZ, float scale, float size_x, float size_y, float size_z, boolean wood, boolean can_stand) {
        this.model = model;
        this.position = position;
        this.rotX = rotX;
        this.rotY = rotY;
        this.rotZ = rotZ;
        this.scale = scale;

        this.SIZE_X = size_x;
        this.SIZE_Y = size_y;
        this.SIZE_Z = size_z;
        this.wood = wood;
        this.DISTANCE_CHECK_COLLISION = Math.max(size_x, size_z) + 1;
        this.can_stand = can_stand;
    }

    public void boundingBox(Player player){      // size or the "radius" on every axis
        Vector3f players_position = player.getPosition();
        float distance = this.distanceFromPlayer(players_position);

        if(distance < this.DISTANCE_CHECK_COLLISION){
            this.player_in_range = true;
            this.distance_from_player = distance;
        }
        else{
            this.player_in_range = false;
        }
    }

    public float distanceFromPlayer(Vector3f players_position){
        float x1 = players_position.x;
        float y1 = players_position.y;
        float z1 = players_position.z;

        float x2 = this.position.x;
        float y2 = this.position.y;
        float z2 = this.position.z;

        return ((float) Math.pow((Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2) + Math.pow(z1 - z2, 2)), 0.5f));
    }


    public void increasePosition(float dx, float dy, float dz){
        this.position.x += dx;
        this.position.y += dy;
        this.position.z += dz;
    }

    public void increaseRotation(float dx, float dy, float dz){
        this.rotX += dx;
        this.rotY += dy;
        this.rotZ += dz;
    }

    public float getTextureXOffset(){
        int column = textureIndex % model.getTexture().getNumberOfRows();
        return (float) column / (float) model.getTexture().getNumberOfRows();
    }

    public float getTextureYOffset(){
        int row = textureIndex / model.getTexture().getNumberOfRows();
        return (float) row / (float) model.getTexture().getNumberOfRows();
    }

    public TexturedModel getModel() {
        return model;
    }

    public void setModel(TexturedModel model) {
        this.model = model;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public float getRotX() {
        return rotX;
    }

    public void setRotX(float rotX) {
        this.rotX = rotX;
    }

    public float getRotY() {
        return rotY;
    }

    public void setRotY(float rotY) {
        this.rotY = rotY;
    }

    public float getRotZ() {
        return rotZ;
    }

    public void setRotZ(float rotZ) {
        this.rotZ = rotZ;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public float getDistance_from_player() {
        return distance_from_player;
    }

    public boolean isPlayer_in_range() {
        return player_in_range;
    }

    public void reduce_hp(int speed){
        double current_time = System.nanoTime() / 1000000.0;
        double diff = current_time - this.previous_time;

        if(this.previous_time == 0 || diff > 200){
            this.previous_time = current_time;
        }
        else{
            this.hp_time = hp_time - (diff * speed);
            this.previous_time = current_time;
        }
    }

    public boolean isWood() {
        return wood;
    }

    public float getSIZE_Y() {
        return SIZE_Y;
    }

    public boolean isCan_stand() {
        return can_stand;
    }

    public float getSIZE_X() {
        return SIZE_X;
    }

    public float getSIZE_Z() {
        return SIZE_Z;
    }

    public boolean isChainsaw() {
        return chainsaw;
    }
}
