package entities;

import models.Model;
import models.TexturedModel;
import objConverter.ModelData;
import objConverter.OBJFileLoader;
import org.lwjgl.util.vector.Vector3f;
import renderingEngine.Loader;
import textures.ModelTexture;

import java.util.List;

public class CirclePicker{
    private TexturedModel model;
    public float x;
    public float y;
    public float z;

    public CirclePicker(Loader loader) {
        ModelData testing = OBJFileLoader.loadOBJ("ring");
        Model raw_log = loader.loadToVAO(testing.getVertices(), testing.getTextureCoords(), testing.getNormals(),
                testing.getIndices());
        this.model = new TexturedModel(raw_log, new ModelTexture(loader.loadTexture("ring")));
    }

    public void placeCircle(List<Entity> entities, float[] position){
        Vector3f vector3f = new Vector3f(position[0], position[1] + 3, position[2]);
        x = position[0];
        y = position[1] + 3;
        z = position[2];

        Entity entity = new Entity(this.getModel(), vector3f, 0,0,
                0,3f, 2,  0,  3, false, false);
        entities.add(entity);
    }


    public void removeCircleAndLight(List<Entity> entities){
        entities.remove(entities.size() - 1);
    }

    public TexturedModel getModel() {
        return model;
    }
}
