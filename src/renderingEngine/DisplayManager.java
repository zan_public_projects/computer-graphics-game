package renderingEngine;

import org.lwjgl.Sys;
import org.lwjgl.opengl.*;
import org.lwjgl.opengl.DisplayMode;

import java.awt.*;

public class DisplayManager {
    private static final String TITLE = "Damm Beavers";

    private static int WIDTH = 1920;      // Values for the display
    private static int HEIGHT = 1080;
    private static final int FPS_MAX = 100;
    private static long  lastFrameTime;
    private static float delta;


    public static void createDisplay(){
        ContextAttribs attribs = new ContextAttribs(3,2)
        .withForwardCompatible(true).withProfileCore(true);
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        WIDTH = gd.getDisplayMode().getWidth();
        HEIGHT = gd.getDisplayMode().getHeight();

        try{
            Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
            Display.create(new PixelFormat(), attribs);
            Display.setTitle(TITLE);
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("error in displayManager");
        }

        GL11.glViewport(0,0, WIDTH, HEIGHT);    // You tell open GL what display to use
        lastFrameTime = getCurrentTime();
    }

    public static void updateDisplay(){
        Display.sync(FPS_MAX);
        Display.update();

        long currentFrameTime = getCurrentTime();
        delta = (currentFrameTime - lastFrameTime) / 1000f;
        lastFrameTime = currentFrameTime;
    }

    public static void closeDisplay(){
        Display.destroy();
    }

    private static long getCurrentTime(){
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }

    public static float getFrameTimeSeconds(){
        return delta;
    }
}
