package A_toRunGame;

import entities.*;
import entities.Entity;
import menuGuis.GuiRenderer;
import menuGuis.GuiTexture;
import models.Model;
import models.TexturedModel;

import objConverter.ModelData;
import objConverter.OBJFileLoader;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import org.lwjgl.util.vector.Vector4f;
import renderingEngine.*;
import terrains.Terrain;
import terrains.TerrainTexture;
import terrains.TerrainTexturePack;
import textures.ModelTexture;
import tools.MouseCoordinates;
import tools.Music;
import tools.RandomTools;
import water.WaterFrameBuffers;
import water.WaterRenderer;
import water.WaterShader;
import water.WaterSquare;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameRunner {
    private static final float LAKE_HEIGHT  = -14f;   // -4 is the highest it can go so from -14 to -4 you can build a dam
    private static final float RIVER_HEIGHT = -14;
    private static final String BACKGROUND_MUSIC = "background_music";
    private static Music music_player = new Music();

    private static List<Entity> entities = new ArrayList<>();
    private static List<Terrain> terrains = new ArrayList<>();
    private static List<Light> lights = new ArrayList<>();
    private static List<GuiTexture> guiTextures = new ArrayList<>();
    private static List<WaterSquare> waters = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException {
        DisplayManager.createDisplay();
        music_player.song_player(BACKGROUND_MUSIC);     // Put in music class
        music_player.loop();

        Loader loader = new Loader();
        MasterRenderer renderer = new MasterRenderer(loader);
        GuiRenderer guiRenderer = new GuiRenderer(loader);
        WaterFrameBuffers water_buffers = new WaterFrameBuffers();
        WaterShader waterShader = new WaterShader();
        WaterRenderer waterRenderer = new WaterRenderer(loader, waterShader, renderer.getProjectionMatrix(), water_buffers);

        // Player
        ModelData beaver_ii = OBJFileLoader.loadOBJ("beaver");
        Model beaver_v2 = loader.loadToVAO(beaver_ii.getVertices(), beaver_ii.getTextureCoords(), beaver_ii.getNormals(),
                beaver_ii.getIndices());
        TexturedModel beaver = new TexturedModel(beaver_v2, new ModelTexture(loader.loadTexture("texture_beaver")));
        Player player = new Player(beaver, new Vector3f(400, -10 , 360), 0 , 0, 0 , 1f);
        Camera camera = new Camera(player);

        initalization(loader);
        fillWaterList();

        // GUI
        GuiTexture logo = new GuiTexture(loader.loadTexture("logo"), new Vector2f(0.8f, 0.8f), new Vector2f(0.2f, 0.3f));
        guiTextures.add(logo);
        GuiTexture first_page = new GuiTexture(loader.loadTexture("first_page"), new Vector2f(0.02f, -0.25f), new Vector2f(1f, 1f));
        GuiTexture black = new GuiTexture(loader.loadTexture("black"), new Vector2f(1f, 1f), new Vector2f(2f, 2f));;
        GuiTexture last_page = new GuiTexture(loader.loadTexture("last_page"), new Vector2f(0.075f, -0.35f), new Vector2f(1.1f, 1.1f));
        guiTextures.add(first_page);

        while(!Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
            guiRenderer.render(guiTextures);
            DisplayManager.updateDisplay();
        }
        guiTextures.remove(1);

        MouseCoordinates mouseCoordinates = new MouseCoordinates(camera, renderer.getProjectionMatrix(), terrains.get(0));
        DammBuilder dammBuilder = new DammBuilder(loader, entities);

        // GUI
        GuiTexture wood_menu = new GuiTexture(loader.loadTexture("inventory_wood"), new Vector2f(-0.85f, -0.8f),
                new Vector2f(0.1f, 0.1f));

        while(!Display.isCloseRequested()){
            camera.move();
            log_placing(mouseCoordinates, player, wood_menu, dammBuilder);
            waterReflectionMaker(renderer, water_buffers, camera);
            closest_wood_entity(player, renderer);

            renderer.renderScene(entities, terrains, lights, camera, new Vector4f(0, -1, 0, 10000));    // w is 10000 just in case
            waterRenderer.render(waters, camera, lights.get(0));
            guiRenderer.render(guiTextures);

            if(dammBuilder.isMaxLogs()){
                break;
            }
            DisplayManager.updateDisplay();
        }
        guiTextures =  new ArrayList<>();
        guiTextures.add(black);
        guiTextures.add(last_page);

        while(!Keyboard.isKeyDown(Keyboard.KEY_SPACE) && dammBuilder.isMaxLogs()){
            guiRenderer.render(guiTextures);
            DisplayManager.updateDisplay();
            Thread.sleep(10);
        }
        clean_and_close(guiRenderer, renderer, loader, water_buffers);
    }


    private static void initalization(Loader loader){
        terrainsFiller(loader);

        // Chainsaw
        ModelData chainsaw_data = OBJFileLoader.loadOBJ("chainsaw");
        Model chainsaw_model = loader.loadToVAO(chainsaw_data.getVertices(), chainsaw_data.getTextureCoords(), chainsaw_data.getNormals(),
                chainsaw_data.getIndices());
        TexturedModel chainsaw = new TexturedModel(chainsaw_model, new ModelTexture(loader.loadTexture("chainsaw")));
        entities.add(new Entity(chainsaw, new Vector3f(671, terrains.get(0).getHeightOfTerrain(671, 258) + 1, 258), true, 0, 1, true, 2, 2));

        for (Terrain terrain_i: terrains) {
            fillEntitiesList(loader, terrain_i);
        }

        Light sun = new Light(new Vector3f(0,20000,0), new Vector3f(0.7f,0.7f,0.7f));
        lights.add(sun);
    }

    private static void log_placing(MouseCoordinates mouseCoordinates, Player player, GuiTexture wood_menu, DammBuilder dammBuilder){
        if(entities.get(0).isChainsaw()){
            entities.get(0).setRotY((entities.get(0).getRotY() + 1) % 360);
        }

        if(player.getWood_count() > 0){
            if(guiTextures.size() < 2){
                guiTextures.add(wood_menu);
            }
            if(Keyboard.isKeyDown(Keyboard.KEY_F)){
                mouseCoordinates.update();
                Vector3f position = player.getPosition();
                float x = position.x;
                float y = position.y;
                float z = position.z;

                float temp_sq = (float) (Math.pow(dammBuilder.circlePicker.x - x, 2) + Math.pow(dammBuilder.circlePicker.y - y, 2)
                        + Math.pow(dammBuilder.circlePicker.z - z, 2));
                float distance_to_circle = (float) Math.pow(temp_sq, 0.5);

                if(distance_to_circle < 6){
                    dammBuilder.placeLogAndNewCircle(entities, waters);
                    player.woodMinusOne();
                }
            }
        }
        else if(guiTextures.size() == 2){
            guiTextures.remove(1);
        }
    }

    private static void waterReflectionMaker(MasterRenderer renderer, WaterFrameBuffers water_buffers, Camera camera){
        GL11.glEnable(GL30.GL_CLIP_DISTANCE0);

        // render reflection
        water_buffers.bindReflectionFrameBuffer();
        float distance = 2 * (camera.getPosition().y - waters.get(0).getHeight());
        camera.getPosition().y -= distance;
        camera.invertPitch();
        renderer.renderScene(entities, terrains, lights, camera, new Vector4f(0, 1, 0, waters.get(0).getHeight()));
        camera.getPosition().y += distance;
        camera.invertPitch();

        // render refraction
        water_buffers.bindRefractionFrameBuffer();
        renderer.renderScene(entities, terrains, lights, camera, new Vector4f(0, -1, 0, waters.get(0).getHeight()));    //  +1 to stop the pixels to poke out on the edges

        // render to screen
        GL11.glDisable(GL30.GL_CLIP_DISTANCE0);
        water_buffers.unbindCurrentFrameBuffer();
    }

    private static void closest_wood_entity(Player player, MasterRenderer renderer){
        float closest = 100000;
        Entity closest_Entity = null;
        int index = 0;

        for (Entity entity : entities) {
            entity.boundingBox(player);
        }

        for (int i = 0; i < entities.size(); i++) {
            if(entities.get(i).isPlayer_in_range()){
                if(entities.get(i).getDistance_from_player() < closest){
                    closest = entities.get(i).getDistance_from_player();
                    closest_Entity = entities.get(i);
                    index = i;
                }
            }
        }

        //TODO
        /*if(closest_Entity != null && closest_Entity.getSIZE_Y() != 0 && !closest_Entity.isCan_stand()){
            Vector3f players_position = player.getPosition();
            float player_x = players_position.x;
            float player_y = players_position.y;
            float player_z = players_position.z;

            Vector3f position = player.getPosition();
            float entity_x = position.x;
            float entity_y = position.y;
            float entity_z = position.z;
            System.out.println("neki");

            if(player_x > (Math.abs(entity_x - closest_Entity.getSIZE_X())) && player_x < Math.abs(entity_x + closest_Entity.getSIZE_X())){
                if(player_z > Math.abs(entity_z - closest_Entity.getSIZE_X()) && player_z < Math.abs(entity_z + closest_Entity.getSIZE_X())){
                   //  player.collisionVector(-1);
                    System.out.println("Collision occured");
                }
            }
        }
        else{
            player.collisionVector(1);
        }*/

        if(closest_Entity != null && closest_Entity.isWood() && Keyboard.isKeyDown(Keyboard.KEY_R) && !closest_Entity.isChainsaw()){
            closest_Entity.reduce_hp(player.getCuttingSpeed(music_player));
            closest_Entity.setRotY((closest_Entity.getRotY() + 1) % 360);

            if(closest_Entity.hp_time <= 0){
                entities.remove(index);
                player.woodPlusOne();
            }
        }
        else if(closest_Entity != null && closest_Entity.isChainsaw() && closest_Entity.isWood() && Keyboard.isKeyDown(Keyboard.KEY_E) ){
            closest_Entity.hp_time = 0;
            entities.remove(0);
            player.setHasChainsaw(true);
        }


        player.move(terrains.get(0), closest_Entity, music_player);
        renderer.processEntity(player);
    }

    private static void terrainsFiller(Loader loader){
        Terrain terrain = new Terrain(0,0,loader, terrainPackMaker(loader, "grass", "dirt", "gravel",
                "tiles"), new TerrainTexture(loader.loadTexture("blendmap")), "heightmap");

        terrains.add(terrain);
    }

    private static TerrainTexturePack terrainPackMaker(Loader loader, String file1, String file2, String file3, String file4){
        TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture(file1));
        TerrainTexture rTexture = new TerrainTexture(loader.loadTexture(file2));
        TerrainTexture gTexture = new TerrainTexture(loader.loadTexture(file3));
        TerrainTexture bTexture = new TerrainTexture(loader.loadTexture(file4));

        return new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
    }

    private static void fillEntitiesList(Loader loader, Terrain terrain){
        Random random = new Random();

        final int[][] HOUSE_COORDINATES = {{580, 164, 35}, {665, 154, 330}, {690, 184, 290}, {710, 230, 300}, {718, 275, 260}, {
                670, 310, 180}, {640, 260, 0}};

        int[][] TREE_COORDINATES_1 = RandomTools.FillPresetCoords("trees1");

        ModelData tree_data = OBJFileLoader.loadOBJ("mattree");
        Model tree_model = loader.loadToVAO(tree_data.getVertices(), tree_data.getTextureCoords(), tree_data.getNormals(),
                tree_data.getIndices());

        ModelData polytree_data = OBJFileLoader.loadOBJ("newtree");
        Model polytree_model = loader.loadToVAO(polytree_data.getVertices(), polytree_data.getTextureCoords(), polytree_data.getNormals(),
                polytree_data.getIndices());

        ModelData house_data = OBJFileLoader.loadOBJ("house");
        Model house_model = loader.loadToVAO(house_data.getVertices(), house_data.getTextureCoords(), house_data.getNormals(),
                house_data.getIndices());

        ModelData fern_data = OBJFileLoader.loadOBJ("fern");
        Model fern_model = loader.loadToVAO(fern_data.getVertices(), fern_data.getTextureCoords(), fern_data.getNormals(),
                fern_data.getIndices());

        ModelTexture fernTextureAtlas = new ModelTexture(loader.loadTexture("fern"));
        fernTextureAtlas.setNumberOfRows(2);

        TexturedModel mattree = new TexturedModel(tree_model, new ModelTexture(loader.loadTexture("mattree")));
        TexturedModel newtree = new TexturedModel(polytree_model, new ModelTexture(loader.loadTexture("newtree")));
        TexturedModel house = new TexturedModel(house_model, new ModelTexture(loader.loadTexture("house")));

        TexturedModel fern = new TexturedModel(fern_model, fernTextureAtlas);
        fern.getTexture().setHasTransparency(true);
        fern.getTexture().setUseFakeLighting(true);


        for(int i = 0; i < 200; i++){
            float x = random.nextFloat() * 790 + 5 + terrain.getX();
            float z = random.nextFloat() * 790 + 5 + terrain.getZ();
            float y = terrain.getHeightOfTerrain(x, z);

            entities.add(new Entity(fern, random.nextInt(4), new Vector3f(x, y, z),0,0,0,0.5f, false));
        }

        for (int[] house_coordinate : HOUSE_COORDINATES) {
            float x = house_coordinate[0];
            float z = house_coordinate[1];
            float y = terrain.getHeightOfTerrain(x, z);

            entities.add(new Entity(house, new Vector3f(x, y, z), house_coordinate[2], 1, false, 7f, 5f, 7f));
        }

        for (int[] tree_coordinate : TREE_COORDINATES_1) {
            float x = tree_coordinate[0];
            float z = tree_coordinate[1];
            float y = terrain.getHeightOfTerrain(x, z) - 1f;

            int tempindex = random.nextInt(2);

            if(tempindex == 0){
                entities.add(new Entity(mattree, new Vector3f(x, y, z),0,random.nextInt() * 360 ,0, 3.2f, true));
            }
            else if (tempindex == 1){
                entities.add(new Entity(newtree, new Vector3f(x, y, z),0,random.nextInt() * 360 ,0, 1.3f, true));
            }
        }
    }

    private static void fillWaterList(){
        WaterSquare lake1 = new WaterSquare(550, 210, LAKE_HEIGHT);
        WaterSquare lake2 = new WaterSquare(450, 210, LAKE_HEIGHT);
        WaterSquare lake3 = new WaterSquare(350, 210, LAKE_HEIGHT);
        WaterSquare lake4 = new WaterSquare(550, 310, LAKE_HEIGHT);
        WaterSquare lake5 = new WaterSquare(450, 310, LAKE_HEIGHT);
        WaterSquare lake6 = new WaterSquare(350, 310, LAKE_HEIGHT);

        WaterSquare river1 = new WaterSquare(415, 410, RIVER_HEIGHT);
        WaterSquare river2 = new WaterSquare(440, 510, RIVER_HEIGHT);
        WaterSquare river3 = new WaterSquare(540, 503, RIVER_HEIGHT);
        WaterSquare river4 = new WaterSquare(540, 603, RIVER_HEIGHT);
        WaterSquare river5 = new WaterSquare(560, 703, RIVER_HEIGHT);
        WaterSquare river6 = new WaterSquare(660, 703, RIVER_HEIGHT);

        waters.add(lake1); waters.add(lake2); waters.add(lake3); waters.add(lake4);
        waters.add(lake5); waters.add(lake6);

        waters.add(river1); waters.add(river2); waters.add(river3); waters.add(river4);
        waters.add(river5); waters.add(river6);
    }

    private static void clean_and_close(GuiRenderer guiRenderer, MasterRenderer renderer, Loader loader,  WaterFrameBuffers fbos){
        fbos.cleanUp();
        guiRenderer.cleanUp();
        renderer.cleanUp();
        loader.cleanUp();
        DisplayManager.closeDisplay();
    }
}