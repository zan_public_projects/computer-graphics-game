package tools;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import javax.sound.sampled.*;

public class Music {
    private Clip clip;
    private String current_song;

    public void song_player(String fileName) {
        try {
            current_song = fileName;
            File file = new File("./res/sound/" + fileName + ".wav");
            if (file.exists()) {
                AudioInputStream sound = AudioSystem.getAudioInputStream(file);
                clip = AudioSystem.getClip();
                clip.open(sound);
                FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
                gainControl.setValue(-43.0f);       // Reduce volume by 43 decibels
            }
            else {
                throw new RuntimeException("Sound: file not found: " + fileName);
            }
        }

        catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Sound: Input/Output Error: " + e);
        }
        catch (LineUnavailableException e) {
            e.printStackTrace();
            throw new RuntimeException("Sound: Line Unavailable Exception Error: " + e);
        }
        catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        }
    }

    public void play(){
        clip.setFramePosition(0);  // Must always rewind!
        clip.start();
    }
    public void loop(){
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }
    public void stop(){
        clip.stop();
    }

    public String getCurrent_song() {
        return current_song;
    }
}