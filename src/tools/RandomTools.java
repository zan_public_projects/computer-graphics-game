package tools;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

import terrains.Terrain;


//Load random coordinates into a list from min and max Y and X coordinates
//*currently not used*
public class RandomTools {
    public static float[][] FillCoords(int size, int minX, int maxX, int minY, int maxY, Random random, Terrain terrain) {
        float[][] list = new float[size][3];
        for(int i = 0; i < size; i++){
            float x = random.nextFloat() * maxX + minX + terrain.getX();
            float z = random.nextFloat() * maxY + minY + terrain.getZ();

            list[i][0] = x;
            list[i][1] = z;
            list[i][2] = 0;
        }
    return list;
    }

//Load tree coordinates  into a list from a .txt file containing X and Y coordinates
    public static int[][] FillPresetCoords(String file){
        int count = 0;
        int[][] array = new int[600][2];
        Scanner sc = null;
        try {
            sc = new Scanner(new File("res/textfiles/" + file + ".txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while(sc.hasNextLine()){
            String coords = sc.nextLine();
            String[] s = coords.split(",");
            array[count][0] = Integer.parseInt(s[0]);
            array[count][1] = Integer.parseInt(s[1]);
            count++;
        }
        int[][] arraycopy = new int[count][2];
        System.arraycopy(array, 0, arraycopy, 0,count);

        return arraycopy;
    }
}
